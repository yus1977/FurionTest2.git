﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Furion.Logging;

public class FileLoggingHelper
{
*//*    /// <summary>
    /// 创建默认FileLoggerProvider，日志文件默认为应用根目录下的app.log
    /// </summary>
    /// <returns></returns>
    public static FileLoggerProvider CreateDefaultFileLoggerProvider(string filename = "application.log",bool append = false)
    {
        *//*        FileLoggerOptions fileLoggerOptions = new FileLoggerOptions
                {
                    Append = append,
                };

                return new FileLoggerProvider(filename, fileLoggerOptions);*//*

        var loggerFactory = LoggerFactory.Create(builder =>
        {
            // 添加默认控制台输出
            builder.AddFile(filename, append);
        });

        return loggerFactory.CreateLogger("Default");
    }*//*

    /// <summary>
    /// 创建默认Logger，日志文件默认为应用根目录下的app.log
    /// </summary>
    /// <returns></returns>
    public static ILogger CreateDefaultLogoger(string filename = "application.log", bool append = false)
    {
        //return CreateDefaultFileLoggerProvider(filename, append)?.CreateLogger("Default");
        *//*        var loggerFactory = LoggerFactory.Create(builder =>
                {
                    // 添加默认控制台输出
                    builder.AddFile(filename, append);
                });

                return loggerFactory.CreateLogger("Default");*//*

        var file = new FileLoggerProvider(filename, append);
        
        return file.CreateLogger("Default");
    }
}
*/