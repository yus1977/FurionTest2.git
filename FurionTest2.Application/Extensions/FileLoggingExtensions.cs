﻿using Furion.DependencyInjection;
using Furion.Extensitions.EventBus;
using Furion.LinqBuilder;
using Microsoft.Extensions.Logging;
using StackExchange.Profiling.Internal;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Furion.Logging;

/// <summary>
/// FileLogging 扩展类
/// </summary>
[SuppressSniffer]
public static class FileLoggingExtensions
{
    /// <summary>
    /// 注册按日滚动的日志文件,每天创建一个日志文件.
    /// 从配置文件中读取Logging:File的配置内容
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configure">更多的配置</param>
    /// <remarks>文件名格式: application-{date}.log</remarks>
    /// <returns></returns>
    public static IServiceCollection AddFileDayRollLogging(this IServiceCollection services, Action<FileLoggerOptions> configure = default)
    {
        return AddFileDayRollLogging(services, "Logging:File", configure);
    }

    /// <summary>
    /// 注册按日滚动的日志文件,每天创建一个日志文件
    /// 从配置文件中读取配置的内容
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuraionKey">配置文件的Key，默认为Logging:File</param>
    /// <param name="configure">更多的配置</param>
    /// <remarks>文件名格式: application-{date}.log</remarks>
    /// <returns></returns>
    public static IServiceCollection AddFileDayRollLogging(this IServiceCollection services
        ,string configuraionKey, Action<FileLoggerOptions> configure = default)
    {
        //默认配置文件的Key为Logging:File
        if (configuraionKey.IsNullOrWhiteSpace()) configuraionKey = "Logging:File";

        services.AddFileLogging(() => configuraionKey, options => 
        {
            options.FileNameRule = DayRollFileNameRule;

            //日志级别过滤
            var maxLevelSetting = App.Configuration[$"{configuraionKey}:MaximumLevel"];
            var maxLevel = TryParseOr(maxLevelSetting, LogLevel.None);

            //是否输出LoggingMonitor
            var isLoggingMonitor = false;
            bool.TryParse(App.Configuration[$"{configuraionKey}:LoggingMonitor"],out isLoggingMonitor);

            //if (maxLevel!= LogLevel.None)
            //{
            options.WriteFilter = (logMsg) =>
            {
                //是否专用于输出监听日志
                if(logMsg.LogName == "System.Logging.LoggingMonitor")
                {
                    if (!isLoggingMonitor) return false;
                }
                else
                {
                    if (isLoggingMonitor) return false;
                }

                    /*                    if (isLoggingMonitor && logMsg.LogName != "System.Logging.LoggingMonitor")
                                        {
                                            return false;
                                        }*/

                    return logMsg.LogLevel >= options.MinimumLevel && logMsg.LogLevel <= maxLevel;
            };                

            //更多的配置
            configure?.Invoke(options);
        });

        return services;
    }

    /// <summary>
    /// 注册按日滚动的日志文件,每天创建一个日志文件
    /// </summary>
    /// <param name="services"></param>
    /// <param name="fileName">文件名,格式: application-{date}.log</param>
    /// <param name="append">追加模式</param>
    /// <param name="minimumLevel">最低日志级别</param>
    /// <param name="configure">更多的配置</param>
    /// <returns></returns>
    public static IServiceCollection AddFileDayRollLogging2(this IServiceCollection services
        , string fileName, bool append = true
        , LogLevel minimumLevel = LogLevel.Information
        , Action<FileLoggerOptions> configure = default)
    {
        services.AddFileLogging(fileName, options =>
        {
            options.Append = append;
            options.MinimumLevel = minimumLevel;
            options.FileNameRule = DayRollFileNameRule;

            //更多的配置
            configure?.Invoke(options);
        });

        return services;
    }

    /// <summary>
    /// 每天创建一个日志文件的规则 
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private static string DayRollFileNameRule(string fileName)
    {
        fileName = fileName.Replace("{date}", "{0:yyyy}-{0:MM}-{0:dd}");
        return string.Format(fileName, DateTime.Now);
    }

    /// <summary>
    /// 字符串转化成枚举值
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="value"></param>
    /// <param name="defaultValue"></param>
    /// <param name="ignoreCase"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    private static T TryParseOr<T>(string value, T defaultValue = default(T), bool ignoreCase = true) where T : struct, IConvertible
    {
        var result = default(T);
        if (Enum.TryParse(value, ignoreCase, out result))
        {
            return result;
        }

        return defaultValue;
    }
}