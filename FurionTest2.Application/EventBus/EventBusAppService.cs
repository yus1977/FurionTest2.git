﻿namespace FurionTest2.Application.EventBus;

/// <summary>
/// TODO事件测试
/// </summary>
[DynamicApiController]
public class ToDoController
{
    // 依赖注入事件发布者 IEventPublisher
    private readonly IEventPublisher _eventPublisher;
    public ToDoController(IEventPublisher eventPublisher)
    {
        _eventPublisher = eventPublisher;
    }

    /// <summary>
    /// 发布 ToDo:Create 消息
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public async Task CreateToDo(string name = "yus")
    {
        await _eventPublisher.PublishAsync("ToDo:Create", name);
    }

    /// <summary>
    /// 发布 ToDo:Update 消息
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public async Task UpdateToDo(string name = "yus")
    {
        await _eventPublisher.PublishAsync("ToDo:Update", name);
    }
}
