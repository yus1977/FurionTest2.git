﻿using Microsoft.Extensions.Logging;

namespace FurionTest2.Application.EventBus;

public class ToDoEventHandlerMonitor : IEventHandlerMonitor
{
    private readonly ILogger<ToDoEventHandlerMonitor> _logger;
    public ToDoEventHandlerMonitor(ILogger<ToDoEventHandlerMonitor> logger)
    {
        _logger = logger;
    }

    /// <summary>
    /// 执行前
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public Task OnExecutingAsync(EventHandlerExecutingContext context)
    {
        _logger.LogInformation("执行之前：{EventId}", context.Source.EventId);
        return Task.CompletedTask;
    }

    /// <summary>
    /// 执行后
    /// </summary>
    /// <param name="context"></param>
    /// <returns></returns>
    public Task OnExecutedAsync(EventHandlerExecutedContext context)
    {
        JsonConvert.DefaultSettings = () => new JsonSerializerSettings
        {
            DateTimeZoneHandling = DateTimeZoneHandling.Local
        };

        _logger.LogInformation("执行之后：{EventId}", context.Source.EventId);
        if (context.Exception != null)
        {
            //这两个的时间都是 UTC, 需要转换一下才能习惯
            var dateTimeKind1 = context.ExecutedTime.Kind;
            var dateTimeKind = context.Source.CreatedTime.Kind;

            _logger.LogError(context.Exception, "执行出错啦, 事件: {Event}", JsonConvert.SerializeObject(context.Source));
        }
        return Task.CompletedTask;
    }
}
