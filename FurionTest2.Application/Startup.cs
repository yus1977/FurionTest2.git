﻿using Furion;
using Furion.Extensions;
using FurionTest2.Application.EventBus;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace FurionTest2.Application;

public class Startup : AppStartup
{
    public void ConfigureServices(IServiceCollection services)
    {
        //控制台日志美化
        services.AddConsoleFormatter();
        //注册按日滚动的日志文件
        services.AddFileDayRollLogging();
        services.AddFileDayRollLogging("Logging:File_Error");
        services.AddFileDayRollLogging("Logging:File_Trace");
        services.AddFileDayRollLogging("Logging:File_Monitor"); //LoggingMonitor 输出文件

        //注入监控日志拦截
        services.AddMonitorLogging();

        // 注册 EventBus 服务
        services.AddEventBus(builder =>
        {
            // 注册 ToDo 事件订阅者
            builder.AddSubscriber<ToDoEventSubscriber>();

            // 添加事件执行监视器
            builder.AddMonitor<ToDoEventHandlerMonitor>();
        });

        //注册远程访问
        services.AddHttpRemote();
    }
}