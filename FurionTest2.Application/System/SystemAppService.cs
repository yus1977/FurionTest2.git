﻿using Furion.FriendlyException;
using FurionTest2.Application.System.Dtos;
using FurionTest2.Application.System.Services;
using Microsoft.AspNetCore.Mvc;

namespace FurionTest2.Application
{
    /// <summary>
    /// 系统服务接口
    /// </summary>
    [DynamicApiController]
    public class SystemAppService
    {
        private readonly ISystemService _systemService;
        private readonly ILogger<ISystemService> _logger;

        [FromServices]
        public SimpleService _simpleService { get; set; }

        public SystemAppService(ISystemService systemService
            , ILogger<ISystemService> logger)
        {
            _systemService = systemService;
            _logger = logger;
        }

        /// <summary>
        /// 获取系统描述,开启日志监控
        /// </summary>
        /// <returns></returns>
        //[LoggingMonitor]
        public string GetDescription()
        {
            return _systemService.GetDescription() + "; SimpleService:" + _simpleService.Name;
        }


        public async void DownloadImg([FromForm]string imgUrl = "https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIymsefAiaI6Zicd88bDTfCSv3g7CjC3ddvfhCCNZ6M20ZIfl0VMIiauYJOK5ZWsoAaDjNeaym8Pb4pw/132")
        {
/*            var imgStram = await imgUrl.get .GetAsStreamAsync();
            var fileSavePath = "D:/appdata/temp/1.jpg";

            using (FileStream fs = File.Create(fileSavePath))
            {
                imgStram.Stream.CopyTo(fs);
                fs.Flush();
            }*/
        }
    }
};