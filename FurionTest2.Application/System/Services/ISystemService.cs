﻿namespace FurionTest2.Application
{
    public interface ISystemService
    {
        string GetDescription();
    }
}