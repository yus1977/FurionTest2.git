﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionTest2.Application.System.Services;

public class SimpleService : ITransient
{
    public string Name { get; set; } = "My Name Is SimpleService";
}
