﻿using Furion.FriendlyException;
using FurionTest2.Application.System.Dtos;

namespace FurionTest2.Application;

/// <summary>
/// Furion 日志测试
/// </summary>
[DynamicApiController]
public class FurionLogAppService
{
    private readonly ILogger<FurionLogAppService> _logger;

    public FurionLogAppService(ILogger<FurionLogAppService> logger)
    {
        _logger = logger;
    }

    /// <summary>
    /// 写入日志-信息
    /// </summary>
    public void WrtieToLogInfo()
    {
        _logger.LogInformation("这是日志测试-Info日志");
    }


    /// <summary>
    /// 写入日志-错误
    /// </summary>
    public void WrtieToLogError()
    {
        _logger.LogError("这是日志测试-Error日志");
    }

    /// <summary>
    /// 写入日志-Debug
    /// </summary>
    public void WrtieToLogDebug()
    {
        _logger.LogDebug("这是日志测试-Debug日志");
    }

    /// <summary>
    /// 写入日志-Trace
    /// </summary>
    public void WrtieToLogTrace()
    {
        _logger.LogTrace("这是日志测试-Trace日志");
    }

    /// <summary>
    /// 抛出业务异常-不记录日志
    /// </summary>
    public void ThrowBah()
    {
        throw Oops.Bah("抛出业务异常");
    }

    /// <summary>
    /// 抛出友好异常-记录日志
    /// </summary>
    public void ThrowOhh()
    {
        throw Oops.Oh("抛出异常");
    }

    /// <summary>
    /// 发生错误异常-记录日志
    /// </summary>
    public void ThrowError()
    {
        int a = 1;
        var b = a / 0;
    }

    /// <summary>
    /// 提交数据,开启日志监控
    /// </summary>
    /// <param name="input"></param>
    //[LoggingMonitor]  //这里不标记了，在配置文件里配置了
    public void PostValidate(InputDto input)
    {

    }
}
