﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FurionTest2.Application.System.Dtos
{
    public class InputDto
    {
        [Required(ErrorMessage ="姓名不能为空!")]
        public string Name { get; set; }

        [MinLength(2,ErrorMessage ="内容不能少于10个字!")]
        public string Text { get; set; }
    }
}
