﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace FurionTest2.EntityFramework.Core
{
    [AppDbContext("FurionTest2", DbProvider.Sqlite)]
    public class DefaultDbContext : AppDbContext<DefaultDbContext>
    {
        public DefaultDbContext(DbContextOptions<DefaultDbContext> options) : base(options)
        {
        }
    }
}