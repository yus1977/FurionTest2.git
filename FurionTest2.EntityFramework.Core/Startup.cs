﻿using Furion;
using Microsoft.Extensions.DependencyInjection;

namespace FurionTest2.EntityFramework.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseAccessor(options =>
            {
                options.AddDbPool<DefaultDbContext>();
            }, "FurionTest2.Database.Migrations");
        }
    }
}